<?php

/**
 * Curse Inc.
 * All Sites
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Dynamic Settings
 * @link		https://gitlab.com/hydrawiki
 *
**/

namespace AllSites\Templates;

use HydraCore;

class TemplateAllSites {
	/**
	 * View the status of a single queued job.
	 *
	 * @access	public
	 * @param	object $wikis Wikis
	 * @param	object $pagination pagination object
	 * @return	string	Built HTML
	 */
	public static function showWikis($wikis, $pagination) {
		$html = $pagination . "
			<div class='allsites'>
				<ul>";
		foreach ($wikis as $wiki) {
			if (!isset($wiki['wiki_domain']) || !isset($wiki['wiki_display_name'])) {
				// Don't display bad entries.
				continue;
			}
			$imgSrc = HydraCore::getWikiImageUrlFromMercury($wiki['md5_key']);
			$html .= "
					<li class='wiki'>
					<a href='https://{$wiki['wiki_domain']}'>
					<img src='{$imgSrc}'>
					<summary>{$wiki['wiki_display_name']}</summary>
					</a>
					</li>
					";
		}
		$html .= "
				</ul>
			</div>
			<div class='clear'></div>
			" . $pagination;
		return $html;
	}
}

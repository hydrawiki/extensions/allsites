<?php

/**
 * Curse Inc.
 * All Sites
 * Count Users Maintenance Cron
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2015 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites\Maintenance;

use DynamicSettings\Wiki;
use AllSites\Jobs\UserCountSync;
use Maintenance;

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class CountUsers extends Maintenance {
	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct();

		$this->mDescription = "Job to queue all wikis for user counting.";
	}

	/**
	 * Trigger all wikis to be counted for users.
	 *
	 * @access	public
	 * @return	void
	 */
	public function execute() {
		$sites = Wiki::loadAll();
		foreach ($sites as $siteKey => $wiki) {
			$this->output("Queueing " . $wiki->getDomains()->getDomain() . " for user counting.\n");
			UserCountSync::queue(['site_key' => $wiki->getSiteKey()]);
		}
	}
}

$maintClass = 'AllSites\\Maintenance\\CountUsers';
require_once RUN_MAINTENANCE_IF_MAIN;

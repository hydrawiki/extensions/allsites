<?php

/**
 * Curse Inc.
 * All Sites
 * Import Site Stats into Config
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2019 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites\Maintenance;

use DynamicSettings\Wiki;
use AllSites\Jobs\UserCountSync;
use Maintenance;

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class importSiteDescriptions extends Maintenance {
	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Job to run once, to import descriptions";
	}

	/**
	 * Trigger all wikis to be counted for users.
	 *
	 * @access	public
	 * @return	void
	 */
	public function execute() {
		$sites = Wiki::loadAll();
		foreach ($sites as $siteKey => $wiki) {
			$wikiDomain = $wiki->getDomain();
			$wikiDomain = str_replace([".wiki",".io"],".com",$wikiDomain);
			echo "Processing $wikiDomain\n";

			try {
				$tags = $this->extract_tags_from_url('https://'.$wikiDomain);
				if(!isset($tags['description'])) {
					$tags['description'] = "";
				}
				$desc = $tags['description'];
				echo "Setting description: $desc\n";
			} catch (exception $e) {
				$desc = "";
				echo "Failed to get description. Skipping.";
				echo $e->getMessage();	
				continue;
			}

			$settings = $wiki->getSettings();
			foreach ($settings as $i => $v) {
				if ($v->getSettingKey() == '$wgWikiDescription') {
					$settings[$i]->setValueFromForm($desc);
				}
			}
			$wiki->saveSettings($settings, "Set Site Description");
		}
	}

	/**
	 * Get a tag from the URL
	 *
	 * @access	public
	 * @return	array
	 */
	public function extract_tags_from_url($url) {
		$tags = [];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$contents = curl_exec($ch);
		curl_close($ch);

		if (empty($contents)) {
			return $tags;
		}

		if (preg_match_all('/<meta([^>]+)content="([^>]+)>/', $contents, $matches)) {
			$doc = new \DOMDocument();
			$doc->loadHTML('<?xml encoding="utf-8" ?>' . implode($matches[0]));
			$tags = [];
			foreach ($doc->getElementsByTagName('meta') as $metaTag) {
				if($metaTag->getAttribute('name') != "") {
					$tags[$metaTag->getAttribute('name')] = $metaTag->getAttribute('content');
				} elseif ($metaTag->getAttribute('property') != "") {
					$tags[$metaTag->getAttribute('property')] = $metaTag->getAttribute('content');
				}
			}
		}

		return $tags;
	}
}

$maintClass = 'AllSites\\Maintenance\\importSiteDescriptions';
require_once RUN_MAINTENANCE_IF_MAIN;

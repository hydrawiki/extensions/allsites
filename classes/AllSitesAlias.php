<?php

/**
 * Curse Inc.
 * AllSites
 * AllSites Aliases
 *
 * @author 		Alexia E. Smith
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AllSites
 * @link		https://gitlab.com/hydrawiki
 *
**/

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [
	'AllSites' => ['AllSites']
];

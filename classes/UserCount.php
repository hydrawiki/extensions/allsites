<?php

/**
 * Curse Inc.
 * AllSites
 * UserCount Class
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2015 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AllSites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites;

use RedisException;
use Wikimedia\Rdbms\DatabaseMysqli;

class UserCount {
	/**
	 * Database Access
	 *
	 * @var		object
	 */
	private $DB;

	/**
	 * Redis Storage
	 *
	 * @var		object
	 */
	private $redis = false;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @param	object $DB MediaWiki Database
	 * @param	object $redis Redis Connection
	 * @return	void
	 */
	public function __construct(DatabaseMysqli $DB, $redis) {
		$this->DB = $DB;

		$this->redis = $redis;
	}

	/**
	 * Push unique users into Redis.
	 *
	 * @access	public
	 * @return	void
	 */
	public function pushUniqueUsers() {
		$result = $this->DB->select(
			['user'],
			['COUNT(*) as total'],
			null,
			__METHOD__
		);
		$total = $result->fetchRow();

		for ($i = 0; $i <= $total['total']; $i = $i + 1000) {
			$results = $this->DB->select(
				['user'],
				[
					'user_name',
					'user_editcount'
				],
				null,
				__METHOD__,
				[
					'OFFSET'	=> $i,
					'LIMIT'		=> 1000
						]
			);

			while ($row = $results->fetchRow()) {
				if (!empty($row['user_name'])) {
					$this->addUniqueName($row['user_name']);
				}

				if (!empty($row['user_name']) && $row['user_editcount'] > 0) {
					$this->addUniqueNameWithEdits($row['user_name']);
				}
			}
		}
	}

	/**
	 * Add a name into the unique names set.
	 *
	 * @access	public
	 * @param	string	$name User Name
	 * @return	void
	 */
	public function addUniqueName($name) {
		try {
			if ($this->redis !== false) {
				$this->redis->sAdd('userCount:uniqueNames', $name);
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Add a name into the unique names with edits set.
	 *
	 * @access	public
	 * @param	string	$name User Name
	 * @return	void
	 */
	public function addUniqueNameWithEdits($name) {
		try {
			if ($this->redis !== false) {
				$this->redis->sAdd('userCount:uniqueNamesWithEdits', $name);
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Return number of unique names.
	 *
	 * @access	public
	 * @return	integer	Number of Unique Names
	 */
	public function getUniqueNamesCount() {
		if ($this->redis === false) {
			return 0;
		}
		try {
			return intval($this->redis->sCard('userCount:uniqueNames'));
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Return number of unique names with edits.
	 *
	 * @access	public
	 * @return	integer	Number of Unique Names that have edits
	 */
	public function getUniqueNamesWithEditsCount() {
		if ($this->redis === false) {
			return 0;
		}
		try {
			return intval($this->redis->sCard('userCount:uniqueNamesWithEdits'));
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}
}

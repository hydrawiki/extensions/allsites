<?php

/**
 * Curse Inc.
 * All Sites
 * Site Information Class
 *
 * @author		Brent Copeland, Tim Aldridge
 * @copyright	(c) 2014 Curse Inc.
 * @license		Proprietary
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites;

use DynamicSettings\Wiki;
use Hydralytics\GoogleAnalytics;
use Hydralytics\Information;
use MWException;
use RedisCache;
use RedisException;
use User;
use HydraCore;
use Wikimedia\Rdbms\DBConnectionError;

class SiteInformation {
	protected $totalStats;
	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		$this->redis = RedisCache::getClient('cache');
	}

	/**
	 * Builds information for all sites and caches it out to Redis and disk.
	 *
	 * @access	public
	 * @return	void
	 */
	public function buildAllSiteStats() {
		$sites = Wiki::loadAll();

		// Reset TotalStats
		$this->totalStats = [
			'ss_total_wikis' => 0,
			'ss_total_edits' => 0,
			'ss_good_articles' => 0,
			'ss_total_pages' => 0,
			'ss_users' => 0,
			'ss_active_users' => 0,
			'ss_images' => 0,
			'ss_unique_users' => 0,
			'ss_unique_contributors' => 0
		];

		// loop through each wiki's stats
		foreach ($sites as $siteKey => $wiki) {
			$data[$siteKey] = $this->buildWikiSiteData($wiki);

			$this->totalStats['ss_total_wikis']++;

			// Sleep 50ms to reduce database load
			usleep(50000);
		}

		// Add Google Analytics when available
		$data = $this->addGoogleAnalytics($sites, $data);

		// Do Global user counts
		$userCounter = new UserCount(wfGetDB(DB_MASTER), $this->redis);
		$this->totalStats['ss_unique_users'] = $userCounter->getUniqueNamesCount();
		$this->totalStats['ss_unique_contributors'] = $userCounter->getUniqueNamesWithEditsCount();

		$data['all'] = $this->totalStats;
		$this->stats = json_encode($data);
		$this->saveSiteStats($this->stats);
	}


	/**
	 * Returns information on all the sites including statistics.
	 *
	 * @access	public
	 * @param	int|null $lastModified [Optional] Unix style time to limit the result set
	 *                                  to the last modified(edited) date of the site.
	 * @param	string|null $siteKey [Optional] Site key to pull data just for the site.
	 * @param	array $filters [Optional] filter data before return.
	 * @param	string|null $depSiteKey [Deprecated] use legacy site key behavior.
	 * @return	mixed	Data array for API response or false on error.
	 */
	public function getSiteStats(
		$lastModified = null,
		$siteKey = null,
		$filters = [],
		$depSiteKey = null
	) {
		global $wgRequest;

		//////////////////////// DEPRECATED ///////////////////////////
		// This is deprecated and will be removed in a later version
		if (!empty($depSiteKey)) {
			return $this->getSiteStatsByWikiDeprecated($depSiteKey);
		}
		//////////////////////// DEPRECATED ///////////////////////////

		$data = [];
		$filters = array_filter((array)$filters);

		// strip first value to remove parent filter
		$parents = array_filter(explode(',', array_shift($filters)));
		$addTotals = (!$parents || in_array('totals', $parents)) ? true : false;
		$addWikis = !$parents || in_array('wikis', $parents) ? true : false;

		$siteStats = $this->getCachedSiteStats();

		if (!$siteStats) {
			throw new AllSitesException(wfMessage('allsites_cache_error')->parse());
		}

		if ($addTotals) {
			$data['totals'] = $this->filterData($siteStats['all'], $filters);
		}
		unset($siteStats['all']);

		if ($addWikis) {
			if (!empty($siteKey)) {
				$siteStats = $this->getSiteStatsByWiki($siteKey, $siteStats);
			}

			foreach ($siteStats as $index => $wiki) {
				if ($lastModified > 0 && $wiki['edited'] < $lastModified) {
					unset($siteStats[$index]);
					// if all sites have been removed return empty wikis array
					if (!$siteStats) {
						$data['wikis'] = [];
						return $data;
					}
					continue;
				}

				if (array_key_exists($siteStats[$index]['portal_domain'], $siteStats)) {
					$siteStats[$index]['portal_domain'] = $siteStats[$siteStats[$index]['portal_domain']]['wiki_domain'];
				}
				if (array_key_exists($siteStats[$index]['group_domain'], $siteStats)) {
					$siteStats[$index]['group_domain'] = $siteStats[$siteStats[$index]['group_domain']]['wiki_domain'];
				}
			}

			$data['wikis'] = $this->filterData(array_values($siteStats), $filters);

			$portal = $wgRequest->getVal('portal');
			if (!empty($portal) && is_array($data['wikis'])) {
				foreach ($data['wikis'] as $key => $wiki) {
					if ($wiki['portal_domain'] != $portal) {
						unset($data['wikis'][$key]);
					}
				}

				// Format consistency for .NET parsing issues.
				$data['wikis'] = array_values($data['wikis']);
			}
		}

		$data = array_filter($data);

		if (!$data) {
			throw new AllSitesException(wfMessage('allsites_filter_error')->parse());
		}

		return $data;
	}

	/**
	 * Build data for an indivdual wiki
	 *
	 * @param Wiki $wiki
	 * @return array
	 */
	private function buildWikiSiteData($wiki) {
		$domain = $wiki->getDomains()->getDomain();

		$data = [
			'wiki_name'			=> $wiki->getName(),
			'wiki_display_name'	=> $wiki->getNameForDisplay(),
			'wiki_domain'		=> $domain,
			'wiki_category'		=> $wiki->getCategory(),
			'wiki_tags'			=> $wiki->getTags(),
			'wiki_image'		=> HydraCore::getWikiImageUrlFromMercury($wiki->getSiteKey(), "large"),
			'db_name'			=> $wiki->getDatabase()['db_name'],
			'portal_master'		=> $wiki->isPortalMaster(),
			'portal_domain'		=> $wiki->getPortalKey(),
			'group_master'		=> $wiki->isGroupMaster(),
			'group_domain'		=> $wiki->getGroupKey(),
			'wiki_language'		=> $wiki->getLanguage(),
			'wiki_managers'		=> $wiki->getManagers(),
			'created'			=> $wiki->getCreated(),
			'edited'			=> $wiki->getEdited(),
			'md5_key'			=> $wiki->getSiteKey(),
			'environment'		=> $_SERVER['PHP_ENV']
		];

		// Include settings we want to publish.
		$data = $this->includeSettings($data, $wiki);

		// Include Site Stats data
		$data = $this->getWikiSiteStats($data, $wiki);

		return $data;
	}

	/**
	 * Get Site Stats for an individual wiki
	 *
	 * @param array $data
	 * @param Wiki $wiki
	 * @return array
	 */
	private function getWikiSiteStats($data, $wiki) {
		$domain = $wiki->getDomains()->getDomain();
		wfDebug(__METHOD__ . " - Pulling statistics for {$domain}...");

		try {
			$lb = $wiki->getDatabaseLB();
			$dbInfo = $wiki->getDatabase();
			$db = $lb->getConnection(DB_REPLICA, false, $dbInfo['db_name']);
		} catch (DBConnectionError $e) {
			wfDebug(__METHOD__ . " - Unable to connect to database: " . $e->getMessage());
			return $data;
		}

		try {
			$result = $db->select(
				['site_stats'],
				['*'],
				null,
				__METHOD__
			);
			$siteStats = $result->fetchRow();
		} catch (MWException $e) {
			wfDebug(__METHOD__ . " - Unable to retrieve site stats.");
			return $data;
		}
		if (!$siteStats['ss_row_id']) {
			wfDebug(__METHOD__ . " - No stats found when checking on {$domain}.");
			return $data;
		}

		unset($siteStats['ss_row_id']);

		// Add site stats to total stats
		foreach ($siteStats as $key => $value) {
			$siteStats[$key] = abs(intval($value));
			$this->totalStats[$key] = intval($this->totalStats[$key] + abs($value));
		}

		try {
			$result = $db->select(
				['revision'],
				['COUNT(DISTINCT rev_user) as users'],
				null,
				__METHOD__
			);
			$contributors = $result->fetchRow();
			$siteStats['ss_contributors'] = abs(intval($contributors['users']));
		} catch (MWException $e) {
			wfDebug(__METHOD__ . " - Unable to retrieve unique contributors");
		}

		// Add User Group Stats to Wiki Stats
		$siteStats['usergroups'] = $this->getUserGroupStats($db);

		// Return the DB to the load balancer
		$lb->closeConnection($db);

		return array_merge($data, $siteStats);
	}

	/**
	 * Include additional settings
	 *
	 * @param array $data
	 * @param Wiki $wiki
	 * @return array
	 */
	private function includeSettings($data, $wiki) {
		$includeSettings = [
			'$wgRightsPage'			=> 'rights_page',
			'$wgRightsIcon'			=> 'rights_icon',
			'$wgRightsText'			=> 'rights_text',
			'$wgRightsUrl'			=> 'rights_url',
			'$wgFavicon'			=> 'favicon',
			'$wgIsOfficialWiki'		=> 'official_wiki',
			'$wgWikiDescription'	=> 'wiki_description',
			'$wgRoadblockNewWiki'	=> 'wiki_crossover'
		];

		foreach ($wiki->getSettings() as $setting) {
			if (array_key_exists($setting->getSettingKey(), $includeSettings)) {
				$value = $setting->getValue();
				switch ($setting->getSettingType()) {
					case 'boolean':
						$value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
						break;
					case 'integer':
						$value = intval($value);
						break;
				}
				if (is_string($value)) {
					$value = str_replace(
						'{$wgUploadPath}',
						'https://' . $data['wiki_domain'] . '/media/' . $data['wiki_domain'],
						$value
					);
				}
				$data[$includeSettings[$setting->getSettingKey()]] = $value;
			}
		}

		return $data;
	}

	/**
	 * Add Google Analytics if available
	 *
	 * @param array $sites
	 * @param array $data
	 * @return array
	 */
	private function addGoogleAnalytics($sites, $data) {
		if (!class_exists('Hydralytics\GoogleAnalytics')) {
			return $data;
		}

		$siteKeys = [];
		// 2018-06-14, Alexia: Hack until we improve DynamicSettings advertisements.
		$siteKeys['8e98826df25deccf6f221a790ee48fda'] = 'UA-35871056-4';

		foreach ($sites as $wiki) {
			// We want raw slot here to avoid getting any roll ups prepended.
			$properties = $wiki->getAdvertisements()->getRawSlot('googleanalyticsid');
			if ($properties !== false && $properties != '') {
				$property = Information::extractGAProperty($properties);
				$siteKeys[$wiki->getSiteKey()] = $property;
			}
		}

		asort($siteKeys);
		$ga = new GoogleAnalytics();
		$ga->loadGAInfo($siteKeys);

		foreach ($data as $key => $wiki) {
			$gaTotalsData = $ga->getTotalsData(strtotime('31 days ago'), strtotime('1 days ago'), $key);
			if ($gaTotalsData['success'] === true) {
				$data[$key]['thirty_day_sessions'] = intval($gaTotalsData['data']['ga:sessions']);
				$data[$key]['thirty_day_views'] = intval($gaTotalsData['data']['ga:pageviews']);
				$data[$key]['thirty_day_users'] = intval($gaTotalsData['data']['ga:users']);
			}
		}

		// 2018-06-14, Alexia: Hack until we improve DynamicSettings advertisements.
		$gaTotalsData = $ga->getTotalsData(
			strtotime('31 days ago'),
			strtotime('1 days ago'),
			'8e98826df25deccf6f221a790ee48fda'
		);

		if ($gaTotalsData['success'] === true) {
			$this->totalStats['thirty_day_sessions'] = intval($gaTotalsData['data']['ga:sessions']);
			$this->totalStats['thirty_day_views'] = intval($gaTotalsData['data']['ga:pageviews']);
			$this->totalStats['thirty_day_users'] = intval($gaTotalsData['data']['ga:users']);
		}

		return $data;
	}

	/**
	 * Push Official Wikis Count to Graphana
	 *
	 * @param array $data Wiki stat data
	 * @return void
	 */
	private function pushOfficialCountToGraphana($data) {
		$count = 0;
		foreach ($data as $d) {
			if ($d['official_wiki']) {
				$count++;
			}
		}

		$conn = fsockopen("monitoring02a-atl.curse.us", 2003);
		$data = "gamepedia.official_wikis ${count} " . time() . "\n";
		fwrite($conn, $data);
		fclose($conn);
	}

	/**
	 * Grabs all the cached stats
	 *
	 * @access	private
	 * @return	mixed	Array of site stats or false on error.
	 */
	private function getCachedSiteStats() {
		global $IP;

		try {
			$siteStats = $this->redis->get('siteStats');
			$siteStats = json_decode($siteStats, true);
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}

		if (!is_array($siteStats['all']) || !count($siteStats['all'])) {
			if (is_file($IP . '/stats/siteStats.json')) {
				$siteStats = file_get_contents($IP . '/stats/siteStats.json');
			}
			if ($siteStats) {
				$siteStats = json_decode($siteStats, true);
			}
		}

		if (!is_array($siteStats['all']) || !count($siteStats['all'])) {
			// Not cached at all?!
			try {
				$this->buildAllSiteStats();
				$siteStats = json_decode($this->stats, true);
			} catch (MWException $e) {
				return false;
			}
		}

		return $siteStats;
	}

	/**
	 * Save all the site stats in Redis and write out a json file
	 *
	 * @access	private
	 * @param	array	$stats array of stats to be cached
	 * @return	void
	 */
	private function saveSiteStats($stats) {
		$status = $this->cacheSiteStats($stats) ? " - Statistics cached to Redis." : " - Redis cache failed!.";
		wfDebug(__METHOD__ . $status);

		$status = $this->writeSiteStats($stats) ? " - Statistics written to disk." : " - Disk write failed!.";
		wfDebug(__METHOD__ . $status);
	}

	/**
	 * Creates site directories.
	 *
	 * @access	private
	 * @param	string	$stats JSON Encoded Statistics
	 * @return	bool	Cache write success
	 */
	private function cacheSiteStats($stats) {
		try {
			$success = $this->redis->set('siteStats', $stats);
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			return false;
		}
		if ($success) {
			return true;
		}
		return false;
	}

	/**
	 * Creates site directories.
	 *
	 * @access	private
	 * @param	string	$stats JSON Encoded Statistics
	 * @return	boolean	File write success
	 */
	private function writeSiteStats($stats) {
		global $IP;

		$success = file_put_contents($IP . '/stats/siteStats.json', $stats);
		if ($success !== false) {
			return true;
		}
		return false;
	}

	/**
	 * Gets stats on a specific wiki
	 *
	 * @access	private
	 * @param	string $siteKey   Wiki Domain
	 * @param   array  $siteStats full stats array
	 * @return	array  Wiki Stats
	 */
	private function getSiteStatsByWiki($siteKey, $siteStats) {
		if (empty($siteStats)) {
			throw new AllSitesException(wfMessage('allsites_cache_error')->parse());
		}

		if ($siteKey == 'all') {
			return $siteStats;
		}

		if (!isset($siteStats[$siteKey])) {
			throw new AllSitesException(wfMessage('allsites_site_key_error')->parse());
		}

		return [$siteKey => $siteStats[$siteKey]];
	}

	/**
	 * [deprecated] Gets stats on a specific wiki
	 *
	 * @access	private
	 * @deprecated version 1
	 * @param	string	$siteKey Wiki Domain
	 * @throws AllSitesException
	 * @return	array	Wiki Stats
	 */
	private function getSiteStatsByWikiDeprecated($siteKey) {
		$siteStats = $this->getCachedSiteStats();

		if (!$siteStats) {
			throw new AllSitesException(wfMessage('allsites_cache_error')->parse());
		}

		if ($siteKey == 'all') {
			return $siteStats;
		}

		if (isset($siteStats[$siteKey])) {
			return $siteStats[$siteKey];
		}

		throw new AllSitesException(wfMessage('allsites_site_key_error')->parse());
	}

	/**
	 * apply filters to data
	 *
	 * @param array $data    full stats array
	 * @param array $filters filters to apply
	 * @return array
	 */
	private function filterData($data, $filters) {
		// if no filter remains just return data
		if (!$filters) {
			return $data;
		}

		// loop through filters return all possible data under filter
		array_map(function ($filter) use (&$data) {
			$filtered = [];
			$keys = array_filter(explode(',', $filter));

			foreach ($keys as $filter) {
				if (isset($data[$filter])) {
					$filtered[$filter] = $data[$filter];
					continue;
				}

				$key = 0;
				array_map(function ($a) use ($filter, &$filtered, &$key) {
					if (isset($a[$filter])) {
						$filtered[$key][$filter] = $a[$filter];
					}
					$key++;
				}, $data);
			}
			$data = $filtered;
		}, $filters);

		return $data;
	}

	/**
	 * Get User Group Stats
	 *
	 * @param object $db
	 * @return array
	 */
	private function getUserGroupStats($db) {
		$allGroups = array_values(User::getAllGroups());
		foreach ($allGroups as $group) {
			$arr['name'] = $group;
			$arr['number'] = $this->numberInGroup($db, $group);
			$groups[] = $arr;
		}
		return $groups;
	}

	/**
	 * Find the number of users in a given user group.
	 * @param string $group Name of group
	 * @return int
	 */
	private function numberInGroup($db, $group) {
		return (int)$db->selectField(
			'user_groups',
			'COUNT(*)',
			[
				'ug_group' => $group,
				'ug_expiry IS NULL OR ug_expiry >= ' . $db->addQuotes($db->timestamp())
			],
			__METHOD__
		);
	}
}

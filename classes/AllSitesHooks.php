<?php

/**
 * Curse Inc.
 * All Sites
 * All Sites Hooks
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites;

use User;
use RedisCache;
use ApiQuerySiteinfo;

class AllSitesHooks {
	/**
	 * Setup extra configuration during MediaWiki setup process.
	 *
	 * @return boolean	True
	 */
	public static function onRegistration() {
		global $wgSyncServices;

		$wgSyncServices["BuildSiteStats"] = "AllSites\\Jobs\\BuildSiteStats";
		$wgSyncServices["UserCountSync"] = "AllSites\\Jobs\\UserCountSync";

		return true;
	}

	/**
	 * Modifies API information.
	 *
	 * @access	public
	 * @param	object $apiQuerySiteinfo ApiQuerySiteinfo object.
	 * @param	array $data Data to modify passed as a reference.
	 * @return	bool
	 */
	public static function onAPIQuerySiteInfoGeneralInfo(ApiQuerySiteinfo $apiQuerySiteinfo, &$data) {
		global $wgFavicon;

		$data['favicon'] = $wgFavicon;

		return true;
	}

	/**
	 * Add unique user information into Redis.
	 *
	 * @access	public
	 * @param	object $user User object.
	 * @param	bool $byEmail If this was created by email or not.
	 * @return	bool
	 */
	public static function onAddNewAccount(User $user, $byEmail) {
		if (PHP_SAPI === 'cli') {
			return true;
		}

		$redis = RedisCache::getClient('cache');

		$userCounter = new UserCount(wfGetDB(DB_MASTER), $redis);

		$name = $user->getName();
		if (!empty($name)) {
			$userCounter->addUniqueName($name);
		}

		return true;
	}

	/**
	 * Add unique user information into Redis now that they have edits.
	 *
	 * @access	public
	 * @param	object $article WikiPage modified
	 * @param	object $user User performing the modification
	 * @param	object $content MW 1.19, Raw Text, MW 1.21 New content, as a Content object
	 * @param	string $summary Edit summary/comment
	 * @param	bool $isMinor Whether or not the edit was marked as minor
	 * @param	bool $isWatch (No longer used)
	 * @param	object $section (No longer used)
	 * @param	int $flags Flags passed to WikiPage::doEditContent()
	 * @param	mixed $revision New Revision of the article
	 * @param	object $status Status object about to be returned by doEditContent()
	 * @param	int $baseRevId the rev ID (or false) this edit was based on
	 * @return	bool
	 */
	public static function onPageContentSaveComplete(
			$article,
			$user,
			$content,
			$summary,
			$isMinor,
			$isWatch,
			$section,
			$flags,
			$revision,
			$status,
			$baseRevId
		) {
		if (PHP_SAPI === 'cli') {
			// return true;
		}

		if ($revision instanceof Revision) {
			$redis = RedisCache::getClient('cache');

			$userCounter = new UserCount(wfGetDB(DB_MASTER), $redis);

			$name = $user->getName();
			if (!empty($name)) {
				$userCounter->addUniqueNameWithEdits($name);
			}
		}

		return true;
	}
}

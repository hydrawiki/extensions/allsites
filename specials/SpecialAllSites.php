<?php

/**
 * Curse Inc.
 * All Sites
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites\Specials;

use HydraCore;
use HydraCore\SpecialPage;
use AllSites\SiteInformation;
use AllSites\Templates\TemplateAllSites;

class SpecialAllSites extends SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var		string
	 */
	private $content;

	/**
	 * Wiki objects storage.
	 *
	 * @var		array
	 */
	private $wikis = [];

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct('AllSites');
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	$subpage Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();
		$this->output->addModuleStyles(['ext.AllSites.styles']);

		$siteInfo = new SiteInformation();
		$sites = $siteInfo->getSiteStats();

		$mode = $this->getRequest()->getText('filter');
		
		$extra = [];

		switch ($mode) {
			case "official":
				$pageTitle = wfMessage('allsites_official');
				$extra += ['filter' => 'official'];
			break;
			default:
				$pageTitle = wfMessage('allsites');
			break;
		}

		$perPage = 250;
		$start = $this->getRequest()->getInt('st');
		$end = $start + $perPage - 1;

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), count($sites['wikis']), $perPage, $start, 4, $extra);

		$wikis = [];
		foreach ($sites['wikis'] as $i => $wiki) {
			if ($i >= $start && $i <= $end) {
				switch ($mode) {
					case "official":
						if ($wiki['official_wiki']) {
							$wikis[] = $wiki;
						}
					break;
					default:
						$wikis[] = $wiki;
					break;
				}
			}
		}

		$this->setHeaders();
		$this->output->setPageTitle($pageTitle);
		$this->content = TemplateAllSites::showWikis($wikis, $pagination);
		$this->output->addHTML($this->content);
	}
}
